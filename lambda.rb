require 'json'
require 'openssl'
require 'acme-client'
require 'aws-sdk-route53'
require 'aws-sdk-dynamodb'

module Function
  class MockCertificateRequest
    def initialize(csr)
      @csr = csr
    end

    def to_der
      @csr
    end
  end

  class Handler
    @acme_server_key = OpenSSL::PKey::RSA.new('-----BEGIN RSA PRIVATE KEY-----
MIII+wIBAAKCAfgAwdVH5CRDcgwVcEaHwW0R9z+YJ/oUUqS67FX6b9YsizrRH6db
y+LuUQpdvH2F3dSitIbDakErfTVYgEtay9n/WO9sQIBTVG9eYpDpLfP7mtDWnifz
RHebIeEGtLNnzlQ51GhN6/QfVd1gAzfsSmfQE3lR40NH2OuTZGod/cQ8btKWhbpp
9WOo/A5LHO6UNB4YolYeIRcMoSSPfPMV8uo3H12wNfaePzuM9cg6KDLQ0FA9jBIY
ErdLMMzkOw0OFSzFn4yL+L8NfjuGUkeTbWrT/JwOqaEdcLJurYaqFObQohp9mZJA
+YUcqw9JIgQPOSMgRfn5Wha5w1w5T9gkqHjH7to/txMrXzrZCQr3XK2UBp+kLfrR
hNZbUI/z/XYFfI5dJeMqP2DewB7GvwaB/7twtqJ6ALz/lJHjJ+lEDf4Xj1S3D2A9
miMSbMbwwOGU7ML0OsLnVs54vISheYjanIM8oM6uXcpfgDh+Ty+321DYuLSUY1S7
gNvnsu7hKikCmSOdusw+uRqpn84ew7uvgmcwm7di4fA7SFduAhZwBcNIqgTfIq6a
jbgrOECqTss8s7gQlRxtUKZF7RQo4CPkrCpx2TFNODquyINndIWpBTzegI89uynK
9zcCxXjlNi5+I/QIYaJ11REo/FkFnLpU8qpRAmn9ucnX+bMCAwEAAQKCAfgAqT5X
2k0Lkt4YLgN3b52TjUY6zdUev5ZUHB1qYRIprJv7kt7nWRvZKGubswQKyclrE6OD
pxgmJ9pXXxXsvoem3HmZFKy3lcb0K64J2/Tn4KKuckPBtiuVCbzYxRjM76w9HkjA
mOB8J9X2inje2YzRct+Ab6YEatQuywOZEkWw2rcHdNZyxC9zkjnMvgNW5jbNHZmC
DzR384/ftaLurD5zSPDd9hBS1jVh/A0LMqREDU+yncYCvPzMK10LfsAEZtrD526p
nNT8rBa/TsJuaKBZXnzIlLY35Xn3B47InR14V8I3nsrD7tqaUIY1Pf07aZFREaiV
X2LuajTxWdTwvO9ICjvNLvfAzRndUvhCnqM274tclPi+NjAN8Rrgui+JMHuGqTwO
lb6tfdjYY0WCQtMes8J1x3hKpgampN/5jIi5WwqCef3+cpZdhG7NrPJTwZXvqiRn
M1AbjNAxi7HPBtsNcTpjrq2QEW/vBueDez2GGnKQz8FuzZxQLFtuanEVYNS1HNJK
bOk2dQzJ5BP6jiaar4A2ghIK709ByWBNzmpR5Lt2R46QlT+2Aiw0D/SVU62ccdTd
WWXgcUZl07SSexje4vBztrtMHRUlv/aukh9DYUUcUCaBxcxL2wl3JBoLsLUQB52R
Y+MP4mBaW5RBRk1aRrAxa53OIXkCgfwOpx4T0vLMwUWmRgUbaXt9fRVwDtQZVxUh
TCOGQaPXwX2e6J+iMzU/WGIPufw9FQUf7Z9t8v3jtGfU6DBNt/6rigN717t4DWZu
5gRz3RhzeXi/ExWwe7lvDtlwzUgvy8WVmZHs89o50vMe24dSDADuNIZNxszvMefs
+HnuOlsUy/AZ09hm8mu+5n8NCeAiHDTc/vdMgF3tIoJy+GN7uIMiurRSAFkZJHkf
Xruo2s2C/kwXRm+g7KA64+CUNWfWMBiVdzds7LphYs4UqxhQovmuZNxNao3qdP8t
QXaL1Fviku+e4fRk9oyAAmJWFEZBvhpbZhDBS8L9JGgYfD0CgfwNOnhkLLVHYfSV
5LwP+xhAzE8OHo3ag9dPGgetBtSEQKhYnqhHAy8nxvZZCl/fGSoeUtjUuun44IUi
KbTwjj/9A/iT46N5p/CiY4Rvugq4HlYS6Z46Mcmp/pB2VmR1vcyrjQBeU2e0CseC
tz15zmXxSjLOTS3mRn/pW2T36BvtBJNXil63GINTfwEmcT1Qt1+H/na8kCeHVlIX
D82lfRS8JPkagGY3Q1+XfDOY+Ka/fM0ysd2NyEBugX99HoQPG2u2f2iNkxGsZTtq
AdrCMEan/2U54t0jodjqzwzooZLSEqZD6vDk5a5bkR923v/g1A9IILH8PvJgCAP7
/K8CgfwKBUnxfrsjQi2yp4IInBui1y8ZAlsUtVQzAFHvF+bHv4B8c1PwZhrNJlFx
tdyFnWFy01vLonj1B0RW+y9Fh0Y349u8Du4bDPb3coCrCBIXroPgEFot5n8LL1mO
5iSu4ylNxMdiB+Y+00AG41TozGK6hNswR3uRPU0B52/jdq8hStL5psIArVr4I8PR
k+ZfDXEtjYRsIgGaEZaqr6nBS48K6io1Rh2oryPSS0QFFxWituwsorPmlsyBWaQV
V2yasyus0+noqQBRZ3PN+LBZPQuJs1ph6zQAmmCEf5dlyyXHjnsm6hnsDhxJ0teT
TKw7e5mk1ekxd/FykzpJPHUCgfwMdaYvtGyKb5kQFgGMi9SsM8PCaMTzrHp/5uST
9Hr2lm/6CHY3bKhdrukZtMvYNruVFDNnKyZ1ZAQByYbDjTh39+WKZj7zt1rPmSyE
j5bJFMm9xo1iAsMPetZWn8i/naXJb2WJcsWarwx78lvU/rLGMjzoU4mH+58/DPZt
ChJDjKu7KHlNu6j2XAmZS8+U1yMD1InB0YVp08Q+NIRd+xIspCZ877NXGf5PkIyI
6KsmcUZVGXK0uIj3jMGn97YrBIjPu/GeZ4ed+Tuc/WIuxYnHcqbFfwkLJsZJY/uk
oYrIkwhHsGJ1NmCjnm0kaaZY/WPna+Q40pE4IlSeY8ECgfwG2OX8BynVlFfr+W6a
cUYPf+kdY1AIMXWK8xyTew1Hwdcffm0HAhaZU8pbk73YWCT1+vXXDhDLVtonHD/V
tM6Cuuf7NfWIkwN2aW1AkVaFkxo4E2bGVT62BljIhxKJpJd6oGKr90T+8KIUXGj9
LWWgObO+7TNfYja5aDLwI+vPoK4O7MRRX5uiIYWzaUbBP5+jzZd5gqMkj+jfAJSe
t95b6dTg0JDC10ojp/fy6+H57Au1u/gi10WKAkRZa7lNguVEf46FpWGArmoUyJjy
fDR2pFKzF7eHL1nWBgT4P9loaSGS8Z2Ken91eP0+BMsp34UZBU7wYTupCH/tB2I=
-----END RSA PRIVATE KEY-----')
    @client = Acme::Client.new(private_key: @acme_server_key, directory: 'https://acme-v02.api.letsencrypt.org/directory', kid: 'https://acme-v02.api.letsencrypt.org/acme/acct/95909198', bad_nonce_retry: 3)

    @dynamodb = Aws::DynamoDB::Client.new
    @route53 = Aws::Route53::Client.new

    def self.putItem(item)
      @dynamodb.put_item({
        item: item, 
        table_name: ENV['PendingCertificatesTableName']
      })
    end

    def self.handleDNSExists(item, challenge)
      dnsPropogationWait = (item['DNSCreatedEpochTime'] + (60 * 5)) > Time.now.to_i
      if (!dnsPropogationWait && !item['IsValidationRequested'])
        challenge.request_validation
        item['IsValidationRequested'] = true;
        self.putItem(item);
      elsif dnsPropogationWait
        return {dnsPropogationWait: dnsPropogationWait, item: item}
      end

      if !item['ChallengeStatus'] || (item['ChallengeStatus'] && item['ChallengeStatus'] != 'valid')
        challenge.reload
        item['ChallengeStatus'] = challenge.status
        item['ChallengeError'] = challenge.error
        self.putItem(item);
      end

      return {dnsPropogationWait: dnsPropogationWait, item: item}
    end

    def self.handleChallengeExists(item, challenge)
      if (!item['IsRoute53Updated'])
        puts "DNS Challenge: #{challenge.record_name} #{challenge.record_type} #{challenge.record_content}"
        resp = @route53.change_resource_record_sets({
          change_batch: {
            changes: [{
              action: "UPSERT",
              resource_record_set: {
                name: item['ChallengeDomainName'],
                resource_records: [{
                  value: "\"#{item['ChallengeRecordContent']}\""
                }],
                ttl: 1,
                type: "TXT"
              },
            }],
            comment: "For ACME letsencrypt. ClientID: #{item['ClientId']}"
          },
          hosted_zone_id: ENV['ROUTE53_HOSTED_ZONE_ID']
        })
        item['IsRoute53Updated'] = true
        item['DNSCreatedEpochTime'] = Time.now.to_i
        self.putItem(item);
      end

      self.handleDNSExists(item, challenge)
    end

    def self.handleNew(clientId, subdomains)
      order = @client.new_order(identifiers: subdomains)
      subdomainsStack = order.identifiers.map{|identifier| identifier['value'] }.reverse
      order.authorizations.map do |authorization|
        challenge = authorization.dns
        subdomain = subdomainsStack.pop
        item = {'ClientId' => clientId, 'AuthorizationUrl' => authorization.url, 'ChallengeUrl' => challenge.url, 'IsRoute53Updated' => false, 'IsValidationRequested' => false, 'CreatedEpoch' => Time.now.to_i, 'DeleteEpoch' => (Time.now + (60 * 60 * 24 * 30)).to_i, 'OrderUrl' => order.url, 'SubdomainsString' => order.identifiers.map{|identifier| identifier['value'] }.sort().join(';'), 'Subdomains' => subdomains, 'Subdomain' => subdomain, 'ChallengeDomainName' => "#{challenge.record_name}.#{subdomain}", 'ChallengeRecordContent' => "#{challenge.record_content}"}
        self.putItem(item);

        self.handleChallengeExists(item, challenge)
      end
    end

    def self.process(event:,context:)
      puts "Event: #{event.to_json}"

      clientId = event["ClientId"]
      puts "ClientID: #{clientId}"
      if !clientId || clientId == ''
        return {resultCode: "FAIL", errorMessage: "Event ClientId must be non-blank"}
      end
      subdomains = event["Subdomains"]
      if !subdomains || subdomains.empty?
        return {resultCode: "FAIL", errorMessage: "Event Subdomains must be non-null and non-empty"}
      end

      results = @dynamodb.query({
        expression_attribute_values: {
          ":clientId" => clientId
        },
        key_condition_expression: "ClientId = :clientId", 
        table_name: ENV['PendingCertificatesTableName']
      })
      if results.items.size > 0
        results.items.each do |item|
          challenge = @client.challenge(url: item['ChallengeUrl'])
          self.handleChallengeExists(item, challenge)
        end
        if results.items.all? { |resultEntry| resultEntry['ChallengeStatus'] == "valid" }
          if !results.items.all? {|item| item['PrivateKey'] }
            private_key = OpenSSL::PKey::RSA.new(4096)
            results.items.each { |item| item['PrivateKey'] = private_key.to_s; self.putItem(item) }
          end
          if !results.items.all?{ |item| item['CSR_Der'] }
            csr = Acme::Client::CertificateRequest.new(private_key: OpenSSL::PKey::RSA.new(results.items.first['PrivateKey']), common_name: results.items.first['Subdomains'].first, names: results.items.first['Subdomains'], subject: {common_name: results.items.first['Subdomains'].first})
            results.items.each { |item| item['CSR_Der'] = Base64.encode64(csr.csr.to_der); self.putItem(item) }
          end
          if !results.items.all?{ |item| item['StatusAfterOrder'] }
            order = @client.order(url: results.items.first['OrderUrl'])
            order.finalize(csr: MockCertificateRequest.new(Base64.decode64(results.items.first['CSR_Der'])))
            status = order.status
            results.items.each { |item| item['StatusAfterOrder'] = status; self.putItem(item) }
          end
          if results.items.any?{ |item| item['StatusAfterOrder'] == 'processing' }
            order = @client.order(url: results.items.first['OrderUrl'])
            status = order.status
            results.items.each { |item| item['StatusAfterOrder'] = status; self.putItem(item) }
          end
          
          if results.items.any? {|item| item['StatusAfterOrder'] != 'processing' && (!item['Certificate'] || !item['CertificateGeneratedEpoch']) }
            order = @client.order(url: results.items.first['OrderUrl'])
            certificate = order.certificate # => PEM-formatted certificate
            results.items.each { |item| item['Certificate'] = certificate; item['CertificateGeneratedEpoch'] = Time.now.to_i; self.putItem(item) }
          end
          
          if results.items.all? {|item| item['Certificate'] && item['CertificateGeneratedEpoch'] && item['PrivateKey'] }
            item = results.items.first
            newItem = {}
            newItem['Subdomains'] = item['SubdomainsString']
            newItem['FullChain'] = item['Certificate']
            newItem['PrivKey'] = item['PrivateKey']
            newItem['expiresEpochTime'] = (Time.now + (60 * 60 * 24 * 60)).to_i
            @dynamodb.put_item({
              table_name: ENV['SubdomainsSSLTableName'],
              item: newItem 
            })
            requestItems = {}
            requestItems[ENV['PendingCertificatesTableName']] = results.items.map { |item| {
              delete_request: {
                key: { 
                  "ClientId" => item['ClientId'],
                  "AuthorizationUrl" => item['AuthorizationUrl'],
                }
              }
            }}
            @dynamodb.batch_write_item({ request_items: requestItems })
            @route53.change_resource_record_sets({
              change_batch: {
                changes: results.items.map{ |item| {
                  action: "DELETE",
                  resource_record_set: {
                    name: item['ChallengeDomainName'],
                    type: "TXT",
                    ttl: 1,
                    resource_records: [{value: "\"#{item['ChallengeRecordContent']}\""}]
                  }
                }}
              },
              hosted_zone_id: ENV['ROUTE53_HOSTED_ZONE_ID']
            })
            return {item: item, certStatus: 'Generated'}
          end
          return {orderStatus: @client.order(url: results.items.first['OrderUrl']).status, results: results.items}       
        else
          return {orderStatus: 'Waiting on challenges', results: results.items}
        end
      else
        self.handleNew(clientId, subdomains);
      end 
    end
  end
end
