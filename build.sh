#!/usr/bin/env bash

bundle install

touch buildArtifact.zip && rm buildArtifact.zip && zip -r buildArtifact.zip ./

aws s3  cp ./buildArtifact.zip  s3://pennidinh-code-assets/pennidinhadminserver-certificatesworker.zip

aws lambda update-function-code --function-name PenniDinhCentral-CertificatesWorker --s3-bucket pennidinh-code-assets --s3-key pennidinhadminserver-certificatesworker.zip --region us-west-2
